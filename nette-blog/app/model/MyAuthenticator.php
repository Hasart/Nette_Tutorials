<?php

namespace App\Model;

use Nette\Security;
use Nette;


/**
 * Created by PhpStorm.
 * User: juras
 * Date: 14.07.2016
 * Time: 14:16
 */
class MyAuthenticator extends Nette\Object implements Nette\Security\IAuthenticator
{
    /** @var Nette\Database\Context $database */
    private $database;


    function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }


    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;

        /** @var \Nette\Database\Table\ActiveRow $row */
        if (!$row = $this->database->table('users')->where('username', $username)->fetch()) {
            throw new Nette\Security\AuthenticationException('User not found.');
        }

        if (!Nette\Security\Passwords::verify($password, $row->password)) {
            throw new Nette\Security\AuthenticationException('Invalid password.');
        }

        return new Nette\Security\Identity($row->id, $row->role, ['username' => $row->username]);
    }
}