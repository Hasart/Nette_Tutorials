<?php
namespace App\Presenters;

use Nette,
    Nette\Application\UI\Form;


class PostPresenter extends BasePresenter
{
    /**
	 * @var Nette\Database\Context
	 * @inject
	 */
    public $database;


    public function renderShow($postId)
	{
		/** @var Nette\Database\Table\ActiveRow $post */
		$post = $this->database->table('posts')->get($postId);

		if (!$post) {
			$this->error('Stránka nebyla nalezena');
		}

		$this->template->database = $this->database->table('comments')->get($postId);
		$this->template->post = $post;
		$this->template->comments = $post->related('comment')->order('created_at')->fetchAll();
        $this->template->comments2 = $post->related('comment')->order('created_at')->fetchAll();


	}


    protected function createComponentCommentForm()
	{
		$form = new Form;

		if (!$this->user->isLoggedIn()) {
			$form->addText('name', 'Jméno:')
				->addRule(Form::MIN_LENGTH, "Jméno musí mít alespoň %s znaky", 3)
				->setRequired("Jméno je povinné pole");

			$form->addText('email', 'Email:')
				->addCondition(Form::FILLED, true)
				->addRule(Form::EMAIL, "Musíte zadat platný email");
		}

		$form->addTextArea('content', 'Komentář:')
			->setRequired("Musíte vyplnit komentář");

		$form->onSubmit[] = function (Form $form) {
			$values = $form->getValues();
			$postId = $this->getParameter('postId');

			if ($this->user->isLoggedIn()) {
				$this->database->table('comments')->insert([
					'post_id' => $postId,
					'name' => $this->user->identity->username,
					'email' => $this->user->getIdentity()->username,
					'content' => $values->content,
				]);
			} else {
				$this->database->table('comments')->insert([
					'post_id' => $postId,
					'name' => $values->name,
					'email' => $values->email,
					'content' => $values->content,
				]);
			}

			$this->flashMessage('Děkuji za komentář', 'success');
			$this->redirect('this');
		};

		return $form;
	}

    protected function createComponentSubCommentForm()
    {
        $form = new Form;
        $form->addHidden('comment_id');
        $form->addTextArea('subcomment', 'subcomment')
            ->setRequired("Musíte vyplnit subkomentář");

        $form->onSubmit[] = function (Form $form) {
            $values = $form->getValues();
            $postId = $this->getParameter('postId');

                $this->database->table('comments')->insert([
                    'post_id' => $postId,
                    'name' => $this->user->identity->username,
                    'email' => $this->user->getIdentity()->username,
                    'content' => $values->subcomment,
                    'comment_id' => $values->comment_id,

                ]);


            $this->flashMessage('Děkuji za komentář', 'success');
            $this->redirect('this');
        };

        return $form;
    }


	protected function createComponentPostForm()
	{
		$form = new Form;
		$form->addText('title', 'Titulek:')
			->setRequired();
		$form->addTextArea('content', 'Obsah:')
			->setRequired();

		$form->addSubmit('send', 'Uložit a publikovat');
		$form->onSuccess[] = [$this, 'postFormSucceeded'];

		return $form;
	}


	public function postFormSucceeded($form, $values)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->error('Pro vytvoření, nebo editování příspěvku se musíte přihlásit.');
		}
		$postId = $this->getParameter('postId');

		if ($postId) {
			$post = $this->database->table('posts')->get($postId);
			$post->update($values);
		} else {
			$post = $this->database->table('posts')->insert($values);
		}

		$this->flashMessage('Příspěvek byl úspěšně publikován.', 'success');
		$this->redirect('show', $post->id);
	}


	public function actionEdit($postId)
		{if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:in');
		}
		$post = $this->database->table('posts')->get($postId);
		if (!$post) {
			$this->error('Příspěvek nebyl nalezen');
		}
		$this['postForm']->setDefaults($post->toArray());

	}
    public function handleSmazKomentar ($id)
    {
        $this->database->table('comments')->where('id', $id)->delete();


    }


	public function actionCreate()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:in');
		}
	}
}