<?php

namespace App\Presenters;

use Nette;
use App\Forms\SignFormFactory;


class SignPresenter extends BasePresenter
{
	/** @var SignFormFactory @inject */
	public $factory;


	/**
	 * @var Nette\Database\Context
	 * @inject
	 */
	public $database;


	public function actionIn()
	{
		if ($this->user->isLoggedIn()) {
			$this->flashMessage("Již jste přihlášeni", "info");
			$this->redirect("Homepage:default");
		}
	}


	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		$form = new Nette\Application\UI\Form;

		$form->addText('username', 'Uživatelské jméno:')
			->setRequired('Prosím vyplňte své uživatelské jméno.');

		$form->addPassword('password', 'Heslo:')
            ->setRequired('Prosím vyplňte své heslo.')
            ->setAttribute("class", "form-control")
            ->setAttribute("placeholder", "Heslo");

		$form->addCheckbox('remember', 'Zůstat přihlášen')
            ->setDefaultValue(false);

		$form->addSubmit('send', 'Přihlásit');

		$form->onSuccess[] = [$this, 'signInFormSucceeded'];
		return $form;
	}


	public function signInFormSucceeded(Nette\Application\UI\Form $form)
	{
		$values = $form->getValues();

		try {
			$this->getUser()->login($values->username, $values->password);
			$this->redirect('Homepage:');

		} catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Nesprávné přihlašovací jméno nebo heslo.', "danger");
            $this->redirect('Sign:in');
			
		}
	}


	public function actionOut()
	{
		$this->getUser()->logout();
	}

}
